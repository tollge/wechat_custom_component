package com.tollge.business.web;

import com.tollge.business.common.Result;
import com.tollge.business.model.BuyerDO;
import com.tollge.business.product.UserProd;
import io.smallrye.mutiny.Uni;
import lombok.extern.log4j.Log4j2;
import org.eclipse.microprofile.config.ConfigProvider;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import org.jboss.resteasy.annotations.jaxrs.PathParam;
import org.jboss.resteasy.annotations.jaxrs.QueryParam;

import javax.inject.Inject;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/buyer")
@Log4j2
@Tag(name = "用户接口组", description = "描述-用户接口")
public class UserWeb {

    @Inject
    UserProd userProd;

    @ConfigProperty(name="useLocalCache")
    private String abc;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("getSingle/{id}")
    @Parameter(name = "id", description = "主键ID", required = true)
    @Parameter(name = "a", description = "测试A")
    @Operation(summary = "接口名", description = "接口描述")
    public Uni<Result<BuyerDO>> single(@PathParam @Min(message="必须大于0", value=1) Long id,
                                         @NotNull @QueryParam String a) {
        log.info(id);
        if (id == 3) {
            throw new RuntimeException();
        }
        return userProd.one(id).map(Result::success);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("getResource/{key}")
    @Parameter(name = "key", description = "描述")
    public Uni<Result<String>> getResource(@PathParam String key) {
        return Uni.createFrom().item(ConfigProvider.getConfig().getValue("useLocalCache", String.class) + "," + abc).map(Result::success);
    }

}