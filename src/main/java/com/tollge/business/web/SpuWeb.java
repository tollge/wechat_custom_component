package com.tollge.business.web;

import com.tollge.business.client.wechat.SpuApi;
import io.smallrye.mutiny.Uni;
import io.vertx.core.json.Json;
import lombok.extern.log4j.Log4j2;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/spu")
@Log4j2
@Tag(name = "spu接口组", description = "描述-spu接口")
public class SpuWeb {

    @Inject
    @RestClient
    SpuApi spuApi;


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("getLogistics")
    @Operation(summary = "接口名", description = "接口描述")
    public Uni<String> single() {
        return spuApi.logsticsList("", "{}").map(Json::encode);
    }

}