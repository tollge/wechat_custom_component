package com.tollge.business.mapper;

import com.tollge.business.model.BuyerDO;
import com.tollge.business.util.DaoUtil;
import com.tollge.sql.SqlSession;
import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import io.smallrye.mutiny.tuples.Tuple2;
import io.vertx.mutiny.sqlclient.RowSet;
import io.vertx.mutiny.sqlclient.Tuple;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;
import java.util.Map;

/**
 *
 * The Table BUYER.
 * BUYER
 */
@ApplicationScoped
public class BuyerDOMapper{

    @Inject
    io.vertx.mutiny.mysqlclient.MySQLPool client;

    public Uni<BuyerDO> one(BuyerDO doName) {
            SqlSession sqlSession = DaoUtil.fetchSql("Buyer.one", doName.toMap());
            return client.preparedQuery(sqlSession.getSql()).execute(Tuple.wrap(sqlSession.getParams()))
                    .onItem().transform(RowSet::iterator)
                    .onItem().transform(iterator -> iterator.hasNext() ? BuyerDO.convertFromRow(iterator.next()) : null);
        }

        public Uni<List<BuyerDO>> list(BuyerDO doName) {
            SqlSession sqlSession = DaoUtil.fetchSql("Buyer.list", doName.toMap());
            return client.preparedQuery(sqlSession.getSql()).execute(Tuple.wrap(sqlSession.getParams()))
                    .onItem().transformToMulti(set -> Multi.createFrom().iterable(set))
                    .onItem().transform(BuyerDO::convertFromRow)
                    .collect().asList();
        }

        public Uni<Long> count(BuyerDO doName) {
            SqlSession sqlSession = DaoUtil.fetchSql("Buyer.count", doName.toMap());
            return client.preparedQuery(sqlSession.getSql()).execute(Tuple.wrap(sqlSession.getParams()))
                    .onItem().transform(pgRowSet -> pgRowSet.iterator().next().getLong(0));
        }

        public Uni<Tuple2<Long, List<BuyerDO>>> page(BuyerDO doName) {
            return Uni.combine().all().unis(count(doName), list(doName)).asTuple();
        }


        public Uni<Long> save(BuyerDO doName) {
            SqlSession sqlSession = DaoUtil.fetchSql("Buyer.save", doName.toMap());
            return client.preparedQuery(sqlSession.getSql()).execute(Tuple.wrap(sqlSession.getParams()))
                    .onItem().transform(pgRowSet -> pgRowSet.iterator().next().getLong("id"));
        }

        public Uni<Boolean> update(BuyerDO from, BuyerDO to) {
            Map<String, Object> fromMap = from.toMap();
            for (Map.Entry<String, Object> entry : to.toMap().entrySet()) {
                fromMap.put("to_"+entry.getKey(), entry.getValue());
            }

            SqlSession sqlSession = DaoUtil.fetchSql("Buyer.update", fromMap);
            return client.preparedQuery(sqlSession.getSql()).execute(Tuple.wrap(sqlSession.getParams()))
                    .onItem().transform(pgRowSet -> pgRowSet.rowCount() > 0);
        }

        public Uni<Boolean> delete(Long id) {
            BuyerDO u = new BuyerDO();
            u.setId(id);
            SqlSession sqlSession = DaoUtil.fetchSql("Buyer.deleteById", u.toMap());
            return client.preparedQuery(sqlSession.getSql()).execute(Tuple.wrap(sqlSession.getParams()))
                    .onItem().transform(pgRowSet -> pgRowSet.rowCount() == 1);
        }
}
