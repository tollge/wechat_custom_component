package com.tollge.business.core;

import com.tollge.business.common.Result;
import lombok.extern.log4j.Log4j2;

import javax.validation.ConstraintViolationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;


/**
 * 校验异常
 */
@Provider
@Log4j2
public class ValidationExceptionHandler implements ExceptionMapper<ConstraintViolationException>
{
    @Override
    public Response toResponse(ConstraintViolationException e)
    {
        return Response.status(Response.Status.OK)
                .entity(
                        Result.error(ErrorCode.PARAMETER_VALIDATE.getCode(),
                                null,
                                e.getMessage() + ErrorCode.PARAMETER_VALIDATE.getReason()).toJsonStr())
                .build();
    }
}