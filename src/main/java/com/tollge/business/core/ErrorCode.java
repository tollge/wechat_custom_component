package com.tollge.business.core;

public enum ErrorCode {
    /**
     * 异常码
     */
    PARAMETER_VALIDATE(1000, "请求入参校验失败"),
    ;

    private final int code;
    private final String reason;

    private ErrorCode(int statusCode, String reasonPhrase) {
        this.code = statusCode;
        this.reason = reasonPhrase;
    }

    public int getCode() {
        return code;
    }

    public String getReason() {
        return reason;
    }
}
