package com.tollge.business.product;

import com.tollge.business.mapper.BuyerDOMapper;
import com.tollge.business.model.BuyerDO;
import io.smallrye.mutiny.Uni;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class UserProd {

    @Inject
    private BuyerDOMapper buyerDOMapper;

    public Uni<BuyerDO> one(Long id) {
        BuyerDO u = new BuyerDO();
        u.setId(id);
        return buyerDOMapper.one(u);
    }
}
