package com.tollge.business.startup;

import io.vertx.core.Vertx;

import javax.enterprise.context.ApplicationScoped;

@io.quarkus.runtime.Startup
@ApplicationScoped
public class Startup {

    Startup() {
        Vertx.vertx().deployVerticle(new SocketServer());
    }

    
}