package com.tollge.business.startup;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.net.NetServer;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class SocketServer extends AbstractVerticle {

    public static final int port = 33323;

    @Override
    public void start() {
        NetServer server = vertx.createNetServer();
        server.connectHandler(socket -> {
            socket.handler(buffer -> {
                System.out.println("Recive:" + buffer.toString());
                socket.write(Buffer.buffer("VertxServer"));
                // 注意，放在executeBlocking中，必须加上future.complete();否则会使服务端给客户端发送失败
                /*vertx.executeBlocking(future -> {
                    socket.write(Buffer.buffer("LongConnection.VertxServer"));
                    future.complete();
                }, asyncResult -> {
                });*/
            });
            socket.closeHandler(close -> log.info("Client Out"));
        });
        server.listen(port, res -> {
            if (res.succeeded()) {
                log.info("Socket server started on " + port);
            }
        });
    }
}