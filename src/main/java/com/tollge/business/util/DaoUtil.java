package com.tollge.business.util;

import com.tollge.sql.SqlSession;
import com.tollge.sql.SqlTemplate;

import java.util.Map;

public class DaoUtil {

    public static SqlSession fetchSql(String sqlKey, Map<String, Object> params) {
        return SqlTemplate.generateSQL(sqlKey, params);
    }

}
