package com.tollge.business.model;

import io.vertx.mutiny.sqlclient.Row;
import lombok.*;
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;
import java.util.Map;

/**
 * The table BUYER
 */
@Getter
@Setter
@NoArgsConstructor
public class BuyerDO{

    /**
     * id 主键.
     */
    private Long id;
    /**
     * channel 渠道.
     */
    @Schema(description = "渠道", required = true, implementation = Long.class)
    private Long channel;
    /**
     * userCode 用户编号.
     */
    private Long userCode;
    /**
     * buyerCode 买家编号.
     */
    private Long buyerCode;
    /**
     * buyerType 买家类型.
     */
    private Long buyerType;
    /**
     * registerTime 注册时间.
     */
    @Schema(description = "注册时间", implementation = LocalDateTime.class, type = SchemaType.STRING)
    private LocalDateTime registerTime;
    /**
     * memberStatus 会员状态.
     */
    private Integer memberStatus;
    /**
     * createTime 创建时间.
     */
    private LocalDateTime createTime;
    /**
     * isDelete 是否删除.
     */
    private Long isDelete;
    /**
     * createBy 创建人.
     */
    @NotBlank
    private String createBy;
    /**
     * updateTime 更新时间.
     */
    private LocalDateTime updateTime;
    /**
     * updateBy 更新人.
     */
    private String updateBy;

    public static BuyerDO convertFromRow(Row next) {
        return null;
    }

    public Map<String, Object> toMap() {
        return null;
    }
}
