package com.tollge.business.common;

import io.vertx.core.json.Json;
import lombok.NoArgsConstructor;
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.media.Schema;


/**
 * @author tollge
 */
@NoArgsConstructor
public class Result<T> {
    @Schema(description = "是否成功", required = true)
    private boolean success;

    @Schema(description = "状态码", required = true)
    private int code;

    @Schema(description = "信息", required = true, type = SchemaType.STRING)
    private String msg;

    @Schema(description = "数据")
    private T data;

    public String toJsonStr() {
        return Json.encode(this);
    }

    private Result(int code, String message) {
        this.code = code;
        this.msg = message;
    }

    private Result(boolean success, int code, T data, String message) {
        this.code = code;
        this.msg = message;
        this.data = data;
        this.success = success;
    }

    public static <T> Result<T> error() {
        return error(500, "未知异常，请联系管理员");
    }

    public static <T> Result<T> error(String message) {
        return error(500, message);
    }

    public static Result error(int code, String message) {
        Result result = new Result(code, message);
        result.setSuccess(false);
        return result;
    }

    public static <T> Result<T> error(int code, T data, String message) {
        return new Result(false, code, data, message);
    }

    public static <T> Result<T> success() {
        return success(null, "处理成功");
    }

    public static <T> Result<T> success(T data) {
        return success(data, "处理成功");
    }

    public static Result succMessage(String message) {
        return success(null, message);
    }

    public static <T> Result<T> success(T data, String message) {
        return new Result(true, 0, data, message);
    }


    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return toJsonStr();
    }
}
