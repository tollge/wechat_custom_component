
package com.tollge.business.client.wechat.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class DeliverySendRequest {

    @JsonProperty("delivery_list")
    private List<DeliveryInfo> deliveryList;
    @JsonProperty("finish_all_delivery")
    private Long finishAllDelivery;
    private String openid;
    @JsonProperty("order_id")
    private Long orderId;
    @JsonProperty("out_order_id")
    private String outOrderId;
}
