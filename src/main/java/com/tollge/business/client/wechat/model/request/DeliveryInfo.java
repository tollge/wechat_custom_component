
package com.tollge.business.client.wechat.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class DeliveryInfo {

    @JsonProperty("delivery_id")
    private String deliveryId;
    @JsonProperty("product_info_list")
    private List<ProductInfo> productInfoList;
    @JsonProperty("waybill_id")
    private String waybillId;

}
