package com.tollge.business.client.wechat.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.tollge.business.client.wechat.model.BaseResponse;
import lombok.Data;

import java.util.List;

@Data
public class SpuListResponse extends BaseResponse {

    @JsonProperty("total_num")
    private Integer totalNum;

    private List<Spu> spus;
}
