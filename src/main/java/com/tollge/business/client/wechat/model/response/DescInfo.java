
package com.tollge.business.client.wechat.model.response;

import lombok.Data;

import java.util.List;

@Data
public class DescInfo {

    private String desc;
    private List<String> imgs;


}
