
package com.tollge.business.client.wechat.model.response;

import com.tollge.business.client.wechat.model.BaseResponse;
import lombok.Data;

@Data
public class SpuAuditResponse extends BaseResponse {

    private SpuAuditData data;

}
