
package com.tollge.business.client.wechat.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class BrandAuditReq {

    @JsonProperty("brand_info")
    private BrandInfo brandInfo;

    /**
     * 营业执照或组织机构代码证，图片url/media_id
     */
    private List<String> license;


}
