
package com.tollge.business.client.wechat.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.tollge.business.client.wechat.model.BaseResponse;
import lombok.Data;

@Data
public class MediaResponse extends BaseResponse {

    @JsonProperty("img_info")
    private ImgInfo imgInfo;

}
