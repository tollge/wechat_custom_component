package com.tollge.business.client.wechat.model.response;

import com.tollge.business.client.wechat.model.BaseResponse;
import com.tollge.business.client.wechat.model.request.SpuRequest;
import lombok.Data;

@Data
public class SpuResponse extends BaseResponse {

    private SpuRequest spu;
}
