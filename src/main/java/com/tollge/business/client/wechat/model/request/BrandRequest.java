
package com.tollge.business.client.wechat.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class BrandRequest {

    @JsonProperty("audit_req")
    private BrandAuditReq auditReq;

}
