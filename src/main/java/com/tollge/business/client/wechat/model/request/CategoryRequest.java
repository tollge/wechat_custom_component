
package com.tollge.business.client.wechat.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class CategoryRequest {

    @JsonProperty("audit_req")
    private CategoryAuditReq auditReq;

}
