
package com.tollge.business.client.wechat.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class CategoryAuditReq {

    @JsonProperty("category_info")
    private CategoryInfo categoryInfo;
    private List<String> license;

}
