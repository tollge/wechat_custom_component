
package com.tollge.business.client.wechat.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.tollge.business.client.wechat.model.BaseResponse;
import lombok.Data;

@Data
public class DeliveryRecieveResquest extends BaseResponse {

    private String openid;
    @JsonProperty("order_id")
    private Long orderId;
    @JsonProperty("out_order_id")
    private String outOrderId;

}
