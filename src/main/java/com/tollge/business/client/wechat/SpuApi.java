package com.tollge.business.client.wechat;

import com.tollge.business.client.wechat.model.request.*;
import com.tollge.business.client.wechat.model.response.*;
import io.smallrye.mutiny.Uni;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.jboss.resteasy.annotations.jaxrs.QueryParam;

import javax.ws.rs.POST;
import javax.ws.rs.Path;


/**
 * 添加商品接口集合
 *
 * @author liutuo
 */
@Path("/shop")
@RegisterRestClient(configKey = "cust-component-api")
public interface SpuApi {
    /**
     * 上传图片
     */
    @POST
    @Path("/img/upload")
    Uni<MediaResponse> imgUpload(@QueryParam("access_token") String authorizerAccessToken, @QueryParam("resp_type") Integer respType, @QueryParam("upload_type") Integer uploadType, @QueryParam("img_url") String imgUrl);

    /**
     * 全量获取商品类目
     * @link https://developers.weixin.qq.com/miniprogram/dev/platform-capabilities/business-capabilities/ministore/minishopopencomponent2/API/cat/get_children_cateogry.html
     */
    @POST
    @Path("/shop/cat/get")
    Uni<CategoryResponse> getAllCategory(@QueryParam("access_token") String authorizerAccessToken, String body);

    /**
     * 品牌审核
     * @link https://developers.weixin.qq.com/miniprogram/dev/platform-capabilities/business-capabilities/ministore/minishopopencomponent2/API/audit/audit_brand.html
     */
    @POST
    @Path("/shop/audit/audit_brand")
    Uni<AuditResponse> auditBrand(@QueryParam("access_token") String authorizerAccessToken, BrandRequest brandRequest);

    /**
     * 类目审核
     * @link https://developers.weixin.qq.com/miniprogram/dev/platform-capabilities/business-capabilities/ministore/minishopopencomponent2/API/audit/audit_category.html
     */
    @POST
    @Path("/shop/audit/audit_category")
    Uni<AuditResponse> auditCategory(@QueryParam("access_token") String authorizerAccessToken, CategoryRequest categoryRequest);

    /**
     * 添加商品
     * @link https://developers.weixin.qq.com/miniprogram/dev/platform-capabilities/business-capabilities/ministore/minishopopencomponent2/API/SPU/add_spu.html
     */
    @POST
    @Path("/shop/spu/add")
    Uni<SpuAuditResponse> addSpu(@QueryParam("access_token") String authorizerAccessToken, SpuRequest spuRequest);

    /**
     * 商品更新
     */
    @POST
    @Path("/shop/spu/update")
    Uni<SpuAuditResponse> updateSpu(@QueryParam("access_token") String authorizerAccessToken, SpuRequest spuRequest);

    /**
     * 获取商品列表
     */
    @POST
    @Path("/shop/spu/get_list")
    Uni<SpuListResponse> spuList(@QueryParam("access_token") String authorizerAccessToken, SpuListRequest spuListRequest);

    /**
     * 获取商品信息
     */
    @POST
    @Path("/shop/spu/get")
    Uni<SpuResponse> spu(@QueryParam("access_token") String authorizerAccessToken, SpuQueryRequest spuQueryRequest);

    /**
     * 商品上架
     */
    @POST
    @Path("/shop/spu/listing")
    Uni<SpuAuditResponse> onsale(@QueryParam("access_token") String authorizerAccessToken, SpuRequest spuRequest);

    /**
     * 商品下架
     */
    @POST
    @Path("/shop/spu/delisting")
    Uni<SpuAuditResponse> offsale(@QueryParam("access_token") String authorizerAccessToken, SpuRequest spuRequest);

    /**
     * 删除商品
     */
    @POST
    @Path("/shop/spu/del")
    Uni<SpuAuditResponse> delSpu(@QueryParam("access_token") String authorizerAccessToken, SpuRequest spuRequest);


    /**
     * 物流公司列表
     */
    @POST
    @Path("/shop/delivery/get_company_list")
    Uni<LogisticCompanyResponse> logsticsList(@QueryParam("access_token") String authorizerAccessToken, String body);


}
