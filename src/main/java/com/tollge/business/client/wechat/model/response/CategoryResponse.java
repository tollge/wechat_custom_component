
package com.tollge.business.client.wechat.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.tollge.business.client.wechat.model.BaseResponse;
import lombok.Data;

import java.util.List;

@Data
public class CategoryResponse extends BaseResponse {

    /**
     * 类目列表
     */
    @JsonProperty("third_cat_list")
    private List<ThirdCatList> thirdCatList;

}
