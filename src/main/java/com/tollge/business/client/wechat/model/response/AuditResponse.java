
package com.tollge.business.client.wechat.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.tollge.business.client.wechat.model.BaseResponse;
import lombok.Data;

@Data
public class AuditResponse extends BaseResponse {

    @JsonProperty("audit_id")
    private String auditId;

}
