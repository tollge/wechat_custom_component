
package com.tollge.business.client.wechat.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.tollge.business.client.wechat.model.BaseResponse;
import lombok.Data;

import java.util.List;

@Data
public class LogisticCompanyResponse extends BaseResponse {

    @JsonProperty("company_list")
    private List<LogisticCompany> data;

}
