package com.tollge.business.client.wechat;


import com.tollge.business.client.wechat.model.BaseResponse;
import com.tollge.business.client.wechat.model.request.AftersaleRequest;
import com.tollge.business.client.wechat.model.request.DeliveryRecieveResquest;
import com.tollge.business.client.wechat.model.request.DeliverySendRequest;
import com.tollge.business.client.wechat.model.request.OrderPayRequest;
import io.smallrye.mutiny.Uni;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.jboss.resteasy.annotations.jaxrs.QueryParam;

import javax.ws.rs.POST;
import javax.ws.rs.Path;

/**
 * 订单接口集合
 *
 * @author liutuo
 */
@Path("/shop")
@RegisterRestClient(configKey = "cust-component-api")
public interface OrderApi {
    /**
     * 同步订单支付结果
     * @link https://developers.weixin.qq.com/miniprogram/dev/platform-capabilities/business-capabilities/ministore/minishopopencomponent2/API/order/pay_order.html
     */
    @POST
    @Path("/shop/order/pay")
    Uni<BaseResponse> orderPay(@QueryParam("access_token") String authorizerAccessToken, OrderPayRequest orderPayRequest);

    /**
     * 创建售后
     * @link https://developers.weixin.qq.com/miniprogram/dev/platform-capabilities/business-capabilities/ministore/minishopopencomponent2/API/aftersale/add.html
     */
    @POST
    @Path("/shop/aftersale/add")
    Uni<BaseResponse> addAftersale(@QueryParam("access_token") String authorizerAccessToken, AftersaleRequest aftersaleRequest);

    /**
     * 更新售后
     * @link https://developers.weixin.qq.com/miniprogram/dev/platform-capabilities/business-capabilities/ministore/minishopopencomponent2/API/aftersale/update.html
     */
    @POST
    @Path("/shop/aftersale/update")
    Uni<BaseResponse> updateAftersale(@QueryParam("access_token") String authorizerAccessToken, AftersaleRequest aftersaleRequest);

    /**
     * 订单发货
     */
    @POST
    @Path("/shop/delivery/send")
    Uni<BaseResponse> deliverySend(@QueryParam("access_token") String authorizerAccessToken, DeliverySendRequest deliverySendRequest);

    /**
     * 订单确认收货
     */
    @POST
    @Path("/shop/delivery/recieve")
    Uni<BaseResponse> deliveryRecieve(@QueryParam("access_token") String authorizerAccessToken, DeliveryRecieveResquest deliveryRecieveResquest);

}
