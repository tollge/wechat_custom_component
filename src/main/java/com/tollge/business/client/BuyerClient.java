package com.tollge.business.client;

import com.tollge.business.common.Result;
import com.tollge.business.model.BuyerDO;
import io.smallrye.mutiny.Uni;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.jboss.resteasy.annotations.jaxrs.PathParam;
import org.jboss.resteasy.annotations.jaxrs.QueryParam;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("/buyer")
@RegisterRestClient(configKey = "tool-man")
public interface BuyerClient {

    @GET
    @Path("getSingle/{id}")
    Uni<Result<BuyerDO>> getSingle(@PathParam Long id, @QueryParam String a);
}
