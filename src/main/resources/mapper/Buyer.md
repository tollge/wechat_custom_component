
## columns
```sub
id,
channel,
user_code,
buyer_code,
buyer_type,
register_time,
member_status,
create_time,
is_delete,
create_by,
update_time,
update_by
```

## conditions
```sub
where 1=1
?if id isNotNull??
    and ID=#id#
if?
?if channel isNotNull??
    and CHANNEL=#channel#
if?
?if userCode isNotNull??
    and USER_CODE=#userCode#
if?
?if buyerCode isNotNull??
    and BUYER_CODE=#buyerCode#
if?
?if buyerType isNotNull??
    and BUYER_TYPE=#buyerType#
if?
?if registerTime isNotNull??
    and REGISTER_TIME=#registerTime#
if?
?if memberStatus isNotNull??
    and MEMBER_STATUS=#memberStatus#
if?
?if createTime isNotNull??
    and CREATE_TIME=#createTime#
if?
?if isDelete isNotNull??
    and IS_DELETE=#isDelete#
if?
?if createBy isNotNull??
    and CREATE_BY=#createBy#
if?
?if updateTime isNotNull??
    and UPDATE_TIME=#updateTime#
if?
?if updateBy isNotNull??
    and UPDATE_BY=#updateBy#
if?
```

## one
> 注释:查询一个
```sql
select ?sub Buyer.columns sub?
from BUYER
?sub Buyer.conditions sub?
limit 1
```

## list
> 注释:查询列表
```sql
select ?sub Buyer.columns sub?
from BUYER
?sub Buyer.conditions sub?
```

## count
> 注释:查询数量
```sql
select count(*)
from BUYER
?sub Buyer.conditions sub?
```

## save
> 注释:保存
```sql
insert into BUYER (?sub Buyer.columns sub?)
values(
#id#,
#channel#,
#userCode#,
#buyerCode#,
#buyerType#,
#registerTime#,
#memberStatus#,
#createTime#,
#isDelete#,
#createBy#,
#updateTime#,
#updateBy#
)
```

## update
> 注释:更新
```sql
update BUYER
set
?if #to_id# isNotNull??
    ID=#to_id#,
if?
?if #to_channel# isNotNull??
    CHANNEL=#to_channel#,
if?
?if #to_userCode# isNotNull??
    USER_CODE=#to_userCode#,
if?
?if #to_buyerCode# isNotNull??
    BUYER_CODE=#to_buyerCode#,
if?
?if #to_buyerType# isNotNull??
    BUYER_TYPE=#to_buyerType#,
if?
?if #to_registerTime# isNotNull??
    REGISTER_TIME=#to_registerTime#,
if?
?if #to_memberStatus# isNotNull??
    MEMBER_STATUS=#to_memberStatus#,
if?
?if #to_createTime# isNotNull??
    CREATE_TIME=#to_createTime#,
if?
?if #to_isDelete# isNotNull??
    IS_DELETE=#to_isDelete#,
if?
?if #to_createBy# isNotNull??
    CREATE_BY=#to_createBy#,
if?
?if #to_updateTime# isNotNull??
    UPDATE_TIME=#to_updateTime#,
if?
?if #to_updateBy# isNotNull??
    UPDATE_BY=#to_updateBy#,
if?
id=id
?sub Buyer.conditions sub?
```

## deleteById
> 注释:根据id删除
```sql
delete from BUYER
where id = #id#
```
